/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pietrzam.pso.functions;

/**
 *
 * @author maciej
 */
public class Rastrigin implements Function {

    public double calculate(double[] x) {
        double wartosc = 0.0;
        for (int i = 0; i < x.length; i++) {
            wartosc += Math.pow(x[i], 2) - Math.cos(18 * Math.PI * x[i]);
        }
        return wartosc;
    }
}
