/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pietrzam.pso.functions;

/**
 * @author maciej
 */
public class Rosenbrock implements Function {

    public double calculate(double[] x) {
        return Math.pow((1 - x[0]), 2) + 100 * Math.pow((x[1] - Math.pow(x[0], 2)), 2);
    }
}
