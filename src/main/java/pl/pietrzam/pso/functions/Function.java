package pl.pietrzam.pso.functions;

/**
 * @author maciej
 */
public interface Function {
    double calculate(double[] coords);
}
