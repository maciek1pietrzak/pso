/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pietrzam.pso.functions;

/**
 * @author maciej
 */
public class Easom implements Function {


    public double calculate(double[] x) {
        return Math.pow(x[0], 2) + 2 * Math.pow(x[1], 2) - 0.3 * Math.cos(3 * Math.PI * x[0])
                - 0.4 * Math.cos(4 * Math.PI * x[1]) + 0.7;
    }
}
