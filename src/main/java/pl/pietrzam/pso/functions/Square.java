/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.pietrzam.pso.functions;

/**
 * @author maciok
 */
public class Square implements Function {

    public double calculate(double[] x) {
        return x[0] * x[0] + x[1] * x[1];
    }
}
