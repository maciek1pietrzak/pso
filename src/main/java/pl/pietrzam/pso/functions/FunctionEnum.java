package pl.pietrzam.pso.functions;

/**
 * @author maciok
 */
public enum FunctionEnum {
    Bohachevsky("Bohachevsky", new Bohachevsky()),
    Easom("Easom", new Easom()),
    Rosenbrock("Rosenbrock", new Rosenbrock()),
    Rastrigin("Rastrigin", new Rastrigin()),
    Square("Square", new Square());

    private String name;
    private Function function;

    FunctionEnum(String val, Function fun) {
        name = val;
        function = fun;
    }

    public Function getFunction() {
        return function;
    }

    @Override
    public String toString() {
        return name;
    }
}
