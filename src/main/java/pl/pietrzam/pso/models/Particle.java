package pl.pietrzam.pso.models;

import pl.pietrzam.pso.algorithm.RandomGenerator;
import pl.pietrzam.pso.criteria.Criteria;
import pl.pietrzam.pso.functions.Function;

/**
 * @author maciok
 */
public class Particle {
    private int dimension;
    private double particleBest;
    private double[] coords;
    private double[] velocity;
    private double[] bestCoords;
    private double[] limitMin;
    private double[] limitMax;

    private double phiGlobal;
    private double phiParticle;

    private Function function;
    private Criteria criteria;

    private RandomGenerator rand = new RandomGenerator();

    /**
     * Konstruktor obiektu
     *
     * @param coords       Współrzędne początkowe
     * @param velocity     Prędkość początkowa
     * @param particleBest Wartość funkcji dla współrzędnych początkowych
     * @param phiGlobal    Parametr określający wpływ ekstremum globalnego
     * @param phiParticle  Parametr określający wpływ ekstremum cząsteczki
     */
    public Particle(double[] coords, double[] velocity, double particleBest, double phiGlobal, double phiParticle) {
        this.dimension = coords.length;
        this.coords = coords;
        this.bestCoords = coords;
        this.velocity = velocity;
        this.particleBest = particleBest;
        this.phiGlobal = phiGlobal;
        this.phiParticle = phiParticle;
    }


    /**
     * Przesuwa cząsteczkę zgodnie z zależnościami
     *
     * @param globalValue  Maksymalna wartość populacji
     * @param globalCoords Współrzędnej najlepszej wartości populacji
     * @param iteration    Liczba wykonanych iteracji
     * @return Otrzymana wartość funkcji dla nowych współrzędnych
     */
    public double move(double globalValue, double[] globalCoords, int iteration) {
        for (int i = 0; i < dimension; i++) {
            double rp = rand.randRange(0, 1);
            double rg = rand.randRange(0, 1);
            double v = velocity[i] / iteration + phiParticle * rp * (bestCoords[i] - coords[i]) +
                    phiGlobal * rg * (globalCoords[i] - coords[i]);
            velocity[i] = v;
            if (coords[i] + v > limitMax[i]) {
                coords[i] = limitMax[i];
            } else if (coords[i] + v < limitMin[i]) {
                coords[i] = limitMin[i];
            } else {
                coords[i] += v;
            }
        }

        double val = function.calculate(coords);
        if (criteria.checkExtreme(val, globalValue)) {
            bestCoords = coords;
            particleBest = val;
        }

        return val;
    }


    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public double getParticleBest() {
        return particleBest;
    }

    public void setParticleBest(double particleBest) {
        this.particleBest = particleBest;
    }

    public double[] getCoords() {
        return coords;
    }

    public void setCoords(double[] coords) {
        this.coords = coords;
    }

    public double[] getVelocity() {
        return velocity;
    }

    public void setVelocity(double[] velocity) {
        this.velocity = velocity;
    }

    public double[] getBestCoords() {
        return bestCoords;
    }

    public void setBestCoords(double[] bestCoords) {
        this.bestCoords = bestCoords;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public Function getFunction() {
        return function;
    }

    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    public void setLimitMin(double[] limitMin) {
        this.limitMin = limitMin;
    }

    public double[] getLimitMin() {
        return limitMin;
    }

    public void setLimitMax(double[] limitMax) {
        this.limitMax = limitMax;
    }

    public double[] getLimitMax() {
        return limitMax;
    }
}
