package pl.pietrzam.pso.criteria;

/**
 * @author maciok
 */
public enum CriteriaEnum {

    MAX_CRITERIA("MaxCriteria", new MaxCriteria()),
    MIN_CRITERIA("MinCriteria", new MinCriteria());

    private String name;
    private Criteria criteria;

    CriteriaEnum(String val, Criteria crit) {
        name = val;
        criteria = crit;
    }

    public Criteria getCriteria() {
        return criteria;
    }

    @Override
    public String toString() {
        return name;
    }

}
