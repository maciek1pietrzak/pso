package pl.pietrzam.pso.criteria;

/**
 * @author maciej
 */
public class MaxCriteria implements Criteria {
    public boolean check(int i, double value) {
        return i > 10000;
    }

    public boolean checkExtreme(double value, double best) {
        return value > best;
    }

    public double getGlobalExtreme() {
        return Double.MIN_VALUE;
    }
}
