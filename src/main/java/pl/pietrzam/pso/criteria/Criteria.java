package pl.pietrzam.pso.criteria;

/**
 * @author maciej
 */
public interface Criteria {
    /**
     * Kryterium zakończenia pracy algorytmu
     *
     * @param i     Liczba iteracji
     * @param value Wartość ekstremum
     * @return Czy zakończyć pracę?
     */
    boolean check(int i, double value);

    /**
     * Kryterium wyboru ekstremum
     *
     * @param value Otrzymana wartość
     * @param best  Aktualna wartość maksymalna
     * @return Czy przewyższa ekstremum?
     */
    boolean checkExtreme(double value, double best);


    /**
     * Zwraca wartość początkową dla ekstremum globalnego
     */
    double getGlobalExtreme();
}

