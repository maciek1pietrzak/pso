package pl.pietrzam.pso.algorithm;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

/**
 * @author maciej
 */
public class RandomGenerator {
    private static final Logger logger = LogManager.getLogger(RandomGenerator.class);
    private Random rand;

    public RandomGenerator() {
        this(System.currentTimeMillis());
    }

    /**
     * Konstruktor umożliwiający zainicjowanie generatora liczb losowych ziarnem
     *
     * @param seed Ziarno, dla tego samego ziarna generator wygeneruje te same wartości
     */
    public RandomGenerator(long seed) {
        rand = new Random();
        logger.info("Inicjuje rand:" + seed);
        rand.setSeed(seed);
    }

    /**
     * Generuje liczbę z przedziału <min, max>
     *
     * @param min Minimalna wartość
     * @param max Maksymalna wartość
     * @return Liczba losowa z danego przedziału
     */
    public double randRange(double min, double max) {
        if (min > max) {
            return -1;
        }
        double random = rand.nextDouble();
        return min + (random * (max - min));
    }

    /**
     * Generuje tablicę liczb losowych o długości podanych tablic. Ich długość musi się zgadzać
     *
     * @param min Tablica minimalnych wartości wymiaru
     * @param max Tablica maksymalnych wartości wymiaru
     * @return Tablica z losowymi liczbami
     */
    public double[] randTable(double[] min, double[] max) {
        if (min.length != max.length) {
            return null;
        }

        double[] table = new double[min.length];
        for (int i = 0; i < min.length; i++) {
            table[i] = randRange(min[i], max[i]);
        }

        return table;
    }
}
