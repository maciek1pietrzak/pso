package pl.pietrzam.pso.algorithm;

import pl.pietrzam.pso.criteria.Criteria;
import pl.pietrzam.pso.functions.Function;
import pl.pietrzam.pso.models.Particle;

/**
 * @author maciej
 */
public class PSO {

    private Particle[] population;
    private double[] bestGlobalCoords;
    private double bestGlobalValue;

    private double[] limitMin;
    private double[] limitMax;

    private double phiGlobal;
    private double phiParticle;
    private int dimension;
    private int populationSize;
    private int iteration;

    private Function function;
    private Criteria criteria;

    private RandomGenerator rand = new RandomGenerator();

    /**
     * Konstruktor obiektu wykonującego obliczenia
     *
     * @param limitMin       Tablica minimalnych wartości wymiarów
     * @param limitMax       Tablica maksymalnych wartości wymiarów
     * @param populationSize Liczebność populacji
     * @param phiGlobal      Parametr określający wpływ ekstremum globalnego
     * @param phiParticle    Parametr określający wpływ ekstremum cząsteczki
     * @param function       Badana funkcja
     * @param criteria       Kryterium zakończenia działania algorytmu
     */
    public PSO(double[] limitMin, double[] limitMax, int populationSize, double phiGlobal, double phiParticle,
               Function function, Criteria criteria) {
        this.limitMin = limitMin;
        this.limitMax = limitMax;
        this.populationSize = populationSize;
        this.phiGlobal = phiGlobal;
        this.phiParticle = phiParticle;
        this.function = function;
        this.criteria = criteria;
        this.bestGlobalValue = criteria.getGlobalExtreme();

        this.dimension = limitMax.length;
    }

    /**
     * Metoda inicjująca populacje danymi losowymi z zakresu
     */
    private void initPopulation() {
        population = new Particle[populationSize];
        double[] startMinVelocity = new double[dimension];
        double[] startMaxVelocity = new double[dimension];
        for (int i = 0; i < dimension; i++) {
            double vel = Math.abs(limitMax[i] - limitMin[i]);
            startMinVelocity[i] = -vel;
            startMaxVelocity[i] = vel;
        }

        for (int i = 0; i < populationSize; i++) {
            double[] randCoords = rand.randTable(limitMin, limitMax);
            double[] randVelocity = rand.randTable(startMinVelocity, startMaxVelocity);
            double result = function.calculate(randCoords);
            Particle par = new Particle(randCoords, randVelocity, result, phiGlobal, phiParticle);
            par.setFunction(function);
            par.setCriteria(criteria);
            par.setLimitMax(limitMax);
            par.setLimitMin(limitMin);

            population[i] = par;

            if (criteria.checkExtreme(result, bestGlobalValue)) {
                bestGlobalValue = result;
                bestGlobalCoords = randCoords;
            }
        }
    }

    private void movePopulation() {
        for (Particle par : population) {
            double value = par.move(bestGlobalValue, bestGlobalCoords, iteration);
            if (criteria.checkExtreme(value, bestGlobalValue)) {
                bestGlobalValue = value;
                bestGlobalCoords = par.getCoords();
            }
        }
        iteration++;
    }

    /**
     * Metoda rozpoczynająca działanie algorytmu
     *
     * @return Ekstremum globalne dla zadanej funkcji
     */
    public double solve() {
        iteration = 0;
        initPopulation();

        while (!criteria.check(iteration, bestGlobalValue)) {
            movePopulation();
        }

        return bestGlobalValue;
    }


    public Particle[] getPopulation() {
        return population;
    }

    public double[] getBestGlobalCoords() {
        return bestGlobalCoords;
    }

    public double getBestGlobalValue() {
        return bestGlobalValue;
    }
}
