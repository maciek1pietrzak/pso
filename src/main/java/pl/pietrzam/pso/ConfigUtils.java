package pl.pietrzam.pso;

import pl.pietrzam.pso.criteria.Criteria;
import pl.pietrzam.pso.criteria.CriteriaEnum;
import pl.pietrzam.pso.functions.Function;
import pl.pietrzam.pso.functions.FunctionEnum;

/**
 * @author maciok
 */
public class ConfigUtils {

    /**
     * Metoda zwracająca odpowiednią funkcję matematyczną
     *
     * @param functionName Nazwa funkcji matematycznej
     * @return Obiekt konkretnej funkcji matematycznej
     */
    public Function getFunction(String functionName) {
        Function function = null;
        for (FunctionEnum funEnum : FunctionEnum.values()) {
            if (funEnum.toString().equals(functionName)) {
                function = funEnum.getFunction();
            }
        }
        return function;
    }

    /**
     * Metoda zwracająca zestaw kryteriów porównawczych
     *
     * @param criteriaName Nazwa kryterium
     * @return Obiekt konkretnego kryterium
     */
    public Criteria getCriteria(String criteriaName) {
        Criteria criteria = null;
        for (CriteriaEnum critEnum : CriteriaEnum.values()) {
            if (critEnum.toString().equals(criteriaName)) {
                criteria = critEnum.getCriteria();
            }
        }
        return criteria;
    }

    /**
     * Metoda dzieląca ciąg znakowy na tablicę double
     *
     * @param params Tablica zapisana w ciągu znakowym
     * @return Tablica typu double
     */
    public double[] split(String params) {
        String[] parameters = params.split(":");
        double[] values = new double[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            values[i] = Double.parseDouble(parameters[i]);
        }
        return values;
    }
}
