package pl.pietrzam.pso;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.pietrzam.pso.algorithm.PSO;
import pl.pietrzam.pso.criteria.Criteria;
import pl.pietrzam.pso.functions.Function;

import java.util.ResourceBundle;

/**
 * @author maciok
 */
public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        ResourceBundle configuration = ResourceBundle.getBundle("config");
        ConfigUtils utils = new ConfigUtils();

        String min = configuration.getString("limitMin");
        String max = configuration.getString("limitMax");
        double[] limitMin = utils.split(min);
        double[] limitMax = utils.split(max);
        int population = Integer.parseInt(configuration.getString("population"));
        double phiGlobal = Double.parseDouble(configuration.getString("phiGlobal"));
        double phiParticle = Double.parseDouble(configuration.getString("phiParticle"));

        Function function = utils.getFunction(configuration.getString("function"));
        Criteria criteria = utils.getCriteria(configuration.getString("criteria"));

        PSO algorithm = new PSO(limitMin, limitMax, population, phiGlobal, phiParticle, function, criteria);
        double value = algorithm.solve();
        double[] coords = algorithm.getBestGlobalCoords();

        logger.info("Znalezione ekstremum wynosi: " + value);
        StringBuffer sb = new StringBuffer(Double.toString(coords[0]));

        for (int i = 1; i < coords.length; i++) {
            sb.append(", ").append(coords[i]);
        }
        logger.info("Znalezione ekstremum leży na współrzędnych: ");
        logger.info(sb.toString());
    }
}
