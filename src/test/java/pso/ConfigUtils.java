package pso;

import org.junit.Test;
import pl.pietrzam.pso.criteria.Criteria;
import pl.pietrzam.pso.criteria.MaxCriteria;
import pl.pietrzam.pso.functions.Bohachevsky;
import pl.pietrzam.pso.functions.Function;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author maciok
 */
public class ConfigUtils {

    private pl.pietrzam.pso.ConfigUtils utils = new pl.pietrzam.pso.ConfigUtils();

    @Test
    public void getCriteriaTest() {
        String criteriaName = "MaxCriteria";
        Criteria criteria = utils.getCriteria(criteriaName);

        assertTrue("Nie udało się pobrać właściwego kryterium", criteria instanceof MaxCriteria);
    }

    @Test
    public void getFunctionTest() {
        String functionName = "Bohachevsky";
        Function fun = utils.getFunction(functionName);

        assertTrue("Nie udało się pobrać właściwego kryterium", fun instanceof Bohachevsky);
    }

    @Test
    public void splitTest() {
        String tableString = "1:1.23:92:0.249";
        double[] doubleTable = {1, 1.23, 92, 0.249};

        double[] table = utils.split(tableString);

        assertArrayEquals("Nie udało się podzielić tablicy", table, doubleTable, 0.0001);

    }
}
