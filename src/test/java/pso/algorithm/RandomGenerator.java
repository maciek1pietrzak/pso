package pso.algorithm;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author maciok
 */
public class RandomGenerator {

    private pl.pietrzam.pso.algorithm.RandomGenerator rand = new pl.pietrzam.pso.algorithm.RandomGenerator();

    @Test
    public void generateRandomDouble1() {
        double max = 15.5;
        double min = 10;
        double random = rand.randRange(min, max);
        assertTrue("Wygenerowano liczbę większą niż zakładano", max > random);
        assertTrue("Wygenerowano liczbę mniejszą niż zakładano", min < random);
    }

    @Test
    public void generateRandomDouble2() {
        double max = -10;
        double min = -234.5;
        double random = rand.randRange(min, max);
        assertTrue("Wygenerowano liczbę większą niż zakładano", max > random);
        assertTrue("Wygenerowano liczbę mniejszą niż zakładano", min < random);
    }

    @Test
    public void generateRandomDouble3() {
        double max = 27.6;
        double min = -12.03;
        double random = rand.randRange(min, max);
        assertTrue("Wygenerowano liczbę większą niż zakładano", max > random);
        assertTrue("Wygenerowano liczbę mniejszą niż zakładano", min < random);
    }

    @Test
    public void generateRandomTable1() {
        double[] min = {12.5, 5, 7};
        double[] max = {24, 12, 27};

        double[] randTable = rand.randTable(min, max);

        for (int i = 0; i < min.length; i++) {
            assertTrue("Wygenerowano liczbę większą niż zakładano", max[i] > randTable[i]);
            assertTrue("Wygenerowano liczbę mniejszą niż zakładano", min[i] < randTable[i]);
        }
    }

    @Test
    public void generateRandomTable2() {
        double[] min = {-12.5};
        double[] max = {76};

        double[] randTable = rand.randTable(min, max);

        for (int i = 0; i < min.length; i++) {
            assertTrue("Wygenerowano liczbę większą niż zakładano", max[i] > randTable[i]);
            assertTrue("Wygenerowano liczbę mniejszą niż zakładano", min[i] < randTable[i]);
        }
    }

    @Test
    public void generateRandomTable3() {
        double[] min = {12.5, 5, 7, 3, 5};
        double[] max = {24, 12, 27, 12, 87.8};

        double[] randTable = rand.randTable(min, max);

        for (int i = 0; i < min.length; i++) {
            assertTrue("Wygenerowano liczbę większą niż zakładano", max[i] > randTable[i]);
            assertTrue("Wygenerowano liczbę mniejszą niż zakładano", min[i] < randTable[i]);
        }
    }

}
